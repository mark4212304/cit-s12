package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){
        return userService.createUser(user);
    }
    @GetMapping("/users")
    public ResponseEntity getUser(@RequestBody User user){
        return new ResponseEntity(userService.getUsers(), HttpStatus.OK);
    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable(name="id") Long id){
        return userService.deleteUser(id);
    }
    @PutMapping("/users/{id}")
    public ResponseEntity updateUser(@PathVariable(name="id")Long id,@RequestBody User user){
        return userService.updateUser(id,user);
    }
}
